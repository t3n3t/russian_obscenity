# -*- encoding: utf-8 -*-
# stub: russian_obscenity 0.0.3 ruby lib config

Gem::Specification.new do |s|
  s.name = "russian_obscenity".freeze
  s.version = "0.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze, "config".freeze]
  s.authors = ["Denis Kurochkin".freeze]
  s.date = "2020-11-18"
  s.email = ["deniskuro@gmail.com".freeze]
  s.files = [".gitignore".freeze, "Gemfile".freeze, "LICENSE.txt".freeze, "README.md".freeze, "Rakefile".freeze, "config/dictionary.yml".freeze, "config/whitelist.yml".freeze, "lib/russian_obscenity.rb".freeze, "lib/russian_obscenity/base.rb".freeze, "lib/russian_obscenity/version.rb".freeze, "russian_obscenity.gemspec".freeze]
  s.homepage = "http://github.com/oranmor/russian_obscenity".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.6.14.1".freeze
  s.summary = "Gem for filtering russian obscene language".freeze

  s.installed_by_version = "2.6.14.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.3"])
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
    else
      s.add_dependency(%q<bundler>.freeze, ["~> 1.3"])
      s.add_dependency(%q<rake>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1.3"])
    s.add_dependency(%q<rake>.freeze, [">= 0"])
  end
end
